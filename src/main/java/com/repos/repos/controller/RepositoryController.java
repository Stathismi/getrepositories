package com.repos.repos.controller;

import com.repos.repos.apiRepository.ApiRepository;
import com.repos.repos.model.RepositoryInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api")
public class RepositoryController implements ApiRepository {

    @GetMapping("/repositories/{userName}")
    public ResponseEntity<?> getRepositories (@PathVariable("userName") final String userName) {
        getGithubRepositoryList(userName);
        return new ResponseEntity<>("test", HttpStatus.OK);
    }

    @Override
    public void getGithubRepositoryList(String userName){
        final String uri = "https://api.github.com/users/" + userName +"/repos";

        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);

        System.out.println(result);
    }

}

//
//    @GetMapping("/repositories/{userName}")
//    public String getRepositories (@PathVariable("userName") final String userName) {
//        System.out.println("--------username is:  " + userName );
//
//        return "Greetings from Spring Boot!";
//    }